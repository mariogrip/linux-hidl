#include <stdio.h>

#define DEBUGI 1

#define ALOGV(...) \
            do { if (DEBUGI) fprintf(stderr, __VA_ARGS__); fprintf(stderr, "\n"); } while (0)

#define ALOGI(...) \
            do { if (DEBUGI) fprintf(stderr, __VA_ARGS__); fprintf(stderr, "\n"); } while (0)

#define ALOGD(...) \
            do { if (DEBUGI) fprintf(stderr, __VA_ARGS__); fprintf(stderr, "\n"); } while (0)

#define ALOGE(...) \
            do { if (DEBUGI) fprintf(stderr, __VA_ARGS__); fprintf(stderr, "\n"); } while (0)

#define ALOGW(...) \
            do { if (DEBUGI) fprintf(stderr, __VA_ARGS__); fprintf(stderr, "\n"); } while (0)

// We should care at some point
#define ALOG_ASSERT(...) do { } while(0)
#define LOG_ALWAYS_FATAL_IF(...) do { } while(0)
#define LOG_FATAL_IF(...) do { } while(0)
#define android_errorWriteLog(...) do { } while(0)
#define LOG_ALWAYS_FATAL(...) do { } while(0)
#define IF_ALOGV(...) do { } while(0)
#define ALOGW_IF(...) do { } while(0)
#define LOG_VERBOSE(...) do { } while(0)
#define IF_ALOG(...) if (false)
#define ALOG(...) do { } while(0)
